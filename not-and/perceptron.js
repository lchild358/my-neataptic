const neataptic = require ( 'neataptic' );
const Serializer = require ( '../serializer.js' );

module.exports = neataptic.Network.merge ( 
  Serializer.fromFile ( `${ __dirname }/and.json` )
, Serializer.fromFile ( `${ __dirname }/not.json` )
);
