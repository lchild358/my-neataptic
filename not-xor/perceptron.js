const neataptic = require ( 'neataptic' );
const Serializer = require ( '../serializer.js' );

module.exports = neataptic.Network.merge ( 
  Serializer.fromFile ( `${ __dirname }/xor.json` )
, Serializer.fromFile ( `${ __dirname }/not.json` )
);
