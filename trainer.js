const args = require ( 'args' );
const neataptic = require ( 'neataptic' );
const fs = require ( 'fs' );
const chalk = require ( 'chalk' );
const sleep = require ( 'system-sleep' );

const Network = neataptic.Network;
const architect = neataptic.architect;
const Perceptron = architect.Perceptron;

args
  .option ( 'dir', 'Path to working directory' )
  .option ( 'training', 'Path to training set file' )
  .option ( 'test', 'Path to test file' )
  .option ( 'output', 'Path to output file' )
  .option ( 'input', 'Path to input file' )
  .option ( 'conf', 'Path to configuration file' )
;

const opts = args.parse ( process.argv );

try{

  let dir = opts.dir ? ( /^\/.+$/.test ( opts.dir ) ? opts.dir : `./${ opts.dir }` ) : __dirname;

  if ( ! opts.conf ) {
    throw 'Configuration not specified';
  }

  let confFile = `${ dir }/${ opts.conf }`;
  if ( ! fs.existsSync ( confFile ) ) {
    throw `Configuration file not found: ${ confFile }`;
  }

  let conf = require ( confFile );
  let nums = conf.perceptron;
  nums.splice ( 0, 0, null );

  let trainingSet = null;
  if ( opts.training ) {
    let trainingFile = `${ dir }/${ opts.training  }`;
    if ( ! fs.existsSync ( trainingFile ) ) {
      throw `Training file not found: ${ trainingFile }`;
    }

    trainingSet = require ( trainingFile );
  }


  let testSet = trainingSet;
  if ( opts.test ) {
    let testFile = `${ dir }/${ opts.test }`;
    if ( ! fs.existsSync ( testFile ) ) {
      throw `Test file not found: ${ testFile }`;
    }

    testSet = require ( testFile );
  }

  let network = nums.length >= 4 ? new ( Function.prototype.bind.apply ( Perceptron, nums ) ) : new ( Function.prototype.bind.apply ( Network, nums ) );

  if ( opts.input ) {
    /* */ console.log ();
    /* */ console.log ( 'Loading input...' );
    let inputFile = `${ dir }/${ opts.input }`;
    if ( fs.existsSync ( inputFile ) ) {
      let jsonData = require ( inputFile );
      let networkObj = Network.fromJSON ( jsonData );

      Object.assign ( network, networkObj );
      /* */ console.log ( chalk.green ( 'Loading done!' ) );
    }
  }

  if ( trainingSet ) {
    let option = conf.training.option;
    let log = option.log ? option.log : 100;
    option.log = undefined;
    option.schedule = {
      iterations : log
    , function : function ( data ) {
      /* */ console.log ( chalk.gray ( '<< ' ), data.iteration, data.error, data.fitness );
      }
    };

    /* */ console.log ();
    /* */ console.log ( 'Training...' );
    
    let doneFlg = false;

    network.evolve ( trainingSet, option )
    .then ( () => {
      /* */ console.log ( chalk.green ( 'Training done!' ) );
      doneFlg = true;
    } );

    while ( true ) {
      sleep ( 100 );
      if ( doneFlg ) break;
    }
  }

  if ( testSet ) {
    /* */ console.log ();
    /* */ console.log ( 'Testing...' );

    for ( var item of testSet ) {
      let input = item.input;
      /* */ console.log ();
      /* */ console.log ( chalk.gray ( '>> ' ), input );
      let result = network.activate ( input );
      /* */ console.log ( chalk.gray ( '<< ' ), result );
    }
    /* */ console.log ( chalk.green ( 'Testing done!' ) );
  }

  if ( opts.output ) {
    /* */ console.log ();
    /* */ console.log ( 'Writing data to file...');
    let outputFile = `${ dir }/${ opts.output }`;
    let json = network.toJSON ();
    fs.writeFileSync ( outputFile, JSON.stringify ( json ) );
    /* */ console.log ( chalk.green ( 'Writing done!' ) );
  }
  /* */ console.log ();
  /* */ console.log ( chalk.green ( 'DONE!' ) );

} catch ( e ) {
  /* */ console.log ( chalk.red ( 'ERROR: '), e );  
}

