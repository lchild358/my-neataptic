const neataptic  = require ( 'neataptic' );
const Network    = neataptic.Network;
const architect  = neataptic.architect;
const Perceptron = architect.Perceptron;

let network = new Network ( 2, 1 );
// let network = new Perceptron ( 2, 3, 1 );

let trainingSet = [
  { input: [ 0, 0 ], output: [ 1 ] }
, { input: [ 0, 1 ], output: [ 0 ] }
, { input: [ 1, 0 ], output: [ 0 ] }
, { input: [ 1, 1 ], output: [ 1 ] }
];

/* */ console.log ( 'Training...' );
network.evolve ( trainingSet, {
  equal : true
, error : 0.03
} ).then ( () => {
  /* */ console.log ( 'Training done!' );

  console.log ( network.activate ( [ 0, 0 ] ) );
  console.log ( network.activate ( [ 1, 0 ] ) );
  console.log ( network.activate ( [ 0, 1 ] ) );
  console.log ( network.activate ( [ 1, 1 ] ) );

  console.log ( network.toJSON () );
} );

