const args = require ( 'args' );
const neataptic = require ( 'neataptic' );
const fs = require ( 'fs' );
const chalk = require ( 'chalk' );

const Network = neataptic.Network;

args
  .option ( 'dir', 'Path to working directory' )
  .option ( 'test', 'Path to test file' )
  .option ( 'output', 'Path to output file' )
  .option ( 'input', 'Path to input file' )
  .option ( 'perceptron', 'Path to perceptron file' )
;

const opts = args.parse ( process.argv );

try{

  let dir = opts.dir ? ( /^\/.+$/.test ( opts.dir ) ? opts.dir : `./${ opts.dir }` ) : __dirname;

  if ( ! opts.perceptron ) {
    throw 'Perceptron not defined';
  }

  let perceptronFile = `${ dir }/${ opts.perceptron }`;
  if ( ! fs.existsSync ( perceptronFile ) ) {
    throw `Perceptron file not found: ${ perceptronFile }`;
  }

  let network = require ( perceptronFile );

  let testSet = null;
  if ( opts.test ) {
    let testFile = `${ dir }/${ opts.test }`;
    if ( ! fs.existsSync ( testFile ) ) {
      throw `Test file not found: ${ testFile }`;
    }

    testSet = require ( testFile );
  }

  if ( opts.input ) {
    /* */ console.log ();
    /* */ console.log ( 'Loading input...' );
    let inputFile = `${ dir }/${ opts.input }`;
    if ( fs.existsSync ( inputFile ) ) {
      let jsonData = require ( inputFile );
      let networkObj = Network.fromJSON ( jsonData );

      Object.assign ( network, networkObj );
      /* */ console.log ( chalk.green ( 'Loading done!' ) );
    }
  }

  if ( testSet ) {
    /* */ console.log ();
    /* */ console.log ( 'Testing...' );

    for ( var item of testSet ) {
      let input = item.input;
      /* */ console.log ();
      /* */ console.log ( chalk.gray ( '>> ' ), input );
      let result = network.activate ( input );
      /* */ console.log ( chalk.gray ( '<< ' ), result );
    }
    /* */ console.log ( chalk.green ( 'Testing done!' ) );
  }

  if ( opts.output ) {
    /* */ console.log ();
    /* */ console.log ( 'Writing data to file...');
    let outputFile = `${ dir }/${ opts.output }`;
    let json = network.toJSON ();
    fs.writeFileSync ( outputFile, JSON.stringify ( json ) );
    /* */ console.log ( chalk.green ( 'Writing done!' ) );
  }
  /* */ console.log ();
  /* */ console.log ( chalk.green ( 'DONE!' ) );

} catch ( e ) {
  /* */ console.log ( chalk.red ( 'ERROR: '), e );  
}

