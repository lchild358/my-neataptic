const neataptic = require ( 'neataptic' );
const fs = require ( 'fs' );

const Network = neataptic.Network;
const Perceptron = neataptic.architect.Perceptron;

const Serializer = function ( sizes ) {
  this.sizes = sizes;

  this.fromFile = function ( filePath ) {

    if ( ! fs.existsSync ( filePath ) ) {
      throw `File not found: ${ filePath }`;
    }

    let jsonData = Network.fromJSON ( require ( filePath ) );
return jsonData;
/*
    let params = this.sizes.map ( val => val );
    params.splice ( 0, 0, null );

    let network = null;
    if ( sizes.length >= 3 ) {
      network = new ( Function.prototype.bind.apply ( Perceptron, params ) );
    } else {
      network = new ( Function.prototype.bind.apply ( Network, params ) );
    }

    Object.assign ( network, jsonData );
*/
  };
};

Serializer.fromFile = function ( filePath ) {

    if ( ! fs.existsSync ( filePath ) ) {
      throw `File not found: ${ filePath }`;
    }

    let jsonData = Network.fromJSON ( require ( filePath ) );
    return jsonData;
};

module.exports = Serializer;

